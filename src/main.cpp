#include "DHT.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>
#include <string>

#define DHTPIN 2     // what digital pin we're connected to

// Uncomment whatever type you're using!
#define DHTTYPE DHT11   // DHT 11

// Connect pin 1 (on the left) of the sensor to +5V
// NOTE: If using a board with 3.3V logic like an Arduino Due connect pin 1
// to 3.3V instead of 5V!
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor

// Initialize DHT sensor.
// Note that older versions of this library took an optional third parameter to
// tweak the timings for faster processors.  This parameter is no longer needed
// as the current DHT reading algorithm adjusts itself to work on faster procs.
DHT dht(DHTPIN, DHTTYPE);

/* SSID & Password */
const char* ssid = "Occam's Router";		// SSID
const char* password = "egmanekkipasswordid";	        // Password

WiFiClient client;

void setup() {

  // // // // // // // // // // // // // // // // // // // // // // // // // // //
  // SENSOR INITIALISING  // // // // // // // // // // // // // // // // // // //
  // // // // // // // // // // // // // // // // // // // // // // // // // // //

  Serial.begin(9600);
  Serial.println("Humiditiy sensor initalising.");
  dht.begin();


  // // // // // // // // // // // // // // // // // // // // // // // // // // //
  // GEARNASON SERVER SETUP  // // // // // // // // // // // // // // // // // //
  // // // // // // // // // // // // // // // // // // // // // // // // // // //

  Serial.begin(9600);
  delay(1000);

  Serial.println("Connecting to ");
  Serial.println(ssid);
  
  //connect to your local wi-fi network
  WiFi.begin(ssid, password);

  //check wi-fi is connected to wi-fi network
  while (WiFi.status() != WL_CONNECTED) {
	delay(500);
	Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
}

void loop() {

  // // // // // // // // // // // // // // // // // // // // // // // // // // //
  // READING TEMP. AND HUM. AT SENSOR // // // // // // // // // // // // // // //
  // // // // // // // // // // // // // // // // // // // // // // // // // // //

  // Wait a few seconds between measurements.
  delay(2000);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();
  // Read temperature as Fahrenheit (isFahrenheit = true)
  float f = dht.readTemperature(true);

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

  // Compute heat index in Fahrenheit (the default)
  float hif = dht.computeHeatIndex(f, h);
  // Compute heat index in Celsius (isFahreheit = false)
  float hic = dht.computeHeatIndex(t, h, false);

  Serial.print("Humidity: ");
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: ");
  Serial.print(t);
  Serial.print(" *C ");
  Serial.print(f);
  Serial.print(" *F\t");
  Serial.print("Heat index: ");
  Serial.print(hic);
  Serial.print(" *C ");
  Serial.print(hif);
  Serial.println(" *F");
  StaticJsonBuffer<200> jsonBuffer;
  JsonObject& root = jsonBuffer.createObject();
  root["humidity"] = h;
  root["temperature"] = t;
  root["heatIndex"] = hic;
  String data;
  root.printTo(data);

  // Okay let's create our json object for sending to GEArnason

  // // // // // // // // // // // // // // // // // // // // // // // // // // //
  // CONNECTING TO GEARNASON SERVER// // // // // // // // // // // // // // // //
  // // // // // // // // // // // // // // // // // // // // // // // // // // //

    if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
 
	    Serial.println("Wifi connect.");

	    HTTPClient http;  //Declare an object of class HTTPClient
	    http.begin("http://192.168.0.72:3000/air_data");  //Specify request destination
	    http.addHeader("Content-Type", "text/plain");
	    int httpCode = http.POST(data); // Send request

	    if (httpCode > 0) { //Check the returning code
	        String payload = http.getString();   //Get the request response payload
	        Serial.println(payload);             //Print the response payload
	    }
 
	    http.end();   //Close connection

	    delay(1000);
 
  }

}
