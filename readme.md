See info at http://docs.platformio.org/en/latest/quickstart.html

// See devices
plaformio device list

// Clean target
platformio run --target clean 

// Compile and upload to target
platformio run --target upload

// Monitor for device, runs in terminal
platformio device monitor
